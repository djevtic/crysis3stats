package com.djevtic.crysis3stats;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

public class PlayerSelect extends Activity{

	private String tag = "djevtic";

	private static final int PROGRESS = 0x1;

	private ProgressBar mProgress;
	private int mProgressStatus = 0;
	Runnable runnable = null;
	Context context = null;
	private ProgressDialog progress ;
	private Player player;
	
	private Button button;
	private ImageView image;

	private Database db;
	private Cursor c;
	private boolean thereArePlayers = false;
	private boolean keyIsPressent = false;

	private String sicretKey = "W7KMmT2uTvD5K0wpdtw42xp6x4ZMR6Mr";
	private String appidentification = "PZlVBFHNyx";

	private StringBuffer something;

		/** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.playerselectlayout);
	        context = getApplicationContext();
	        Bundle extras = getIntent().getExtras();
	        db = new Database(this);
	        if (extras != null) {
	        	boolean value1 = extras.getBoolean("error");
	            if (value1 != true) {
	            	showErrorMessage("We couldn't found player on that specific platform or Crysis 3 Stats are down at moment");
	            }
	        }  
	        addListenerOnButton();
	        isOnline();
	    }

		public void addListenerOnButton() {
			button = (Button) findViewById(R.id.confirmButton);
			button.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					String selectedPlatform = getRadioButon();
					String selectedPlayername = getPlayername();
					if(selectedPlayername!=""&&selectedPlayername!=null&&isOnline()){
						new ProgressDialog(PlayerSelect.this);
						progress = ProgressDialog.show(PlayerSelect.this, "", "Loading...");
						player = Player.getInstance();
				        player.GetPlayer(selectedPlayername, selectedPlatform);
				        try {
				        	synchronized (this) {
				        		this.wait(2000);
				        	}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        if(player.getState()==1){
				        	player.setConnectionProblems(true);
				        }
				        player.setState(1);
				        if(player.getConnectionProblems()){
				        	player.setConnectionProblems(false);
				        	progress.dismiss();
				        	showErrorMessage("We didn't find selected player on selected platform. Please try again.");
				        }else{
				        	new Thread(new Runnable() {
					             public void run() {
					            	 while(!player.weAreGood){	                     
					                 }
					            	 boolean dataGood = player.getData();
					            	 if(dataGood){
					            		 Intent i = new Intent(context, MainActivity.class);
					            		 progress.dismiss();
					            		 startActivity(i);
					            	 }
					             }
					         }).start();
				        }
					}else{
						showErrorMessage("Please fill Player Name and select Platform for which you want statistic to be shown.");
					}
				}
	 
			});
	 
		}

		protected void showErrorMessage(CharSequence connectionErrorMessage) {
			Context context = getApplicationContext();
	        int duration = Toast.LENGTH_LONG;
	        Toast toast = Toast.makeText(context, connectionErrorMessage, duration);
	    	toast.show();
		}

		protected String  getPlayername() { 
			String playerName = "";
			EditText editText = (EditText) findViewById(R.id.editPlayerName);
			playerName = editText.getText().toString();
			editText.length();
			if(editText.length()==0){
				playerName = "";
			}
			return playerName;
		}

		protected String getRadioButon() {
			RadioGroup radioGroup = (RadioGroup) findViewById(R.id.platform);
			int checkedRadioButton = radioGroup.getCheckedRadioButtonId();
			 
			String radioButtonSelected = "";
			 
			if (checkedRadioButton == R.id.pc) {
				radioButtonSelected = "pc";
			} else if (checkedRadioButton == R.id.ps3) {
				radioButtonSelected = "ps3";
			} else if (checkedRadioButton == R.id.xbox) {
				radioButtonSelected = "360";
			}
			return radioButtonSelected;
		}
		
		public boolean isOnline() {
		    ConnectivityManager cm =
		        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		    boolean connection = cm.getActiveNetworkInfo() != null && 
		       cm.getActiveNetworkInfo().isConnectedOrConnecting();
		    if(!connection){
		    	showErrorMessage("For this to work you need internet connection.");
		    }
		    return connection;
		    
		}
		
		@Override
	    public void onBackPressed(){
			db = new Database(this);
			db.open();
			c = db.getAllPlayers();
			if (c.moveToFirst()) {
				thereArePlayers = true;
			}
			c.close();
			db.close();
			if(thereArePlayers){
				Intent playerGeneralStats = new Intent(PlayerSelect.this, PlayerSelector.class);
	            PlayerSelect.this.startActivity(playerGeneralStats);
			}else{
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		}

}
