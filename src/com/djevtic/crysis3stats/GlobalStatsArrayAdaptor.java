package com.djevtic.crysis3stats;

import java.text.DecimalFormat;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.novoda.imageloader.core.ImageManager;
import com.novoda.imageloader.core.LoaderSettings;
import com.novoda.imageloader.core.LoaderSettings.SettingsBuilder;
import com.novoda.imageloader.core.loader.Loader;
import com.novoda.imageloader.core.model.ImageTag;
import com.novoda.imageloader.core.model.ImageTagFactory;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class GlobalStatsArrayAdaptor extends ArrayAdapter<String>{

	private static Context context;
	private String[] values;
	private Player player;
	private String statsName;
	private HashMap<String, String> weaponHash;
	private JSONObject weaponJSON;
	private Database db;
	private ImageManager imageManager;
	private ImageTagFactory imageTagFactory;
	private Loader imageLoader;

	private View rowView;
	private ImageView playerRankImage;
	private TextView playerName;
	private TextView playerRank;
	private TextView playerProgress;
	private Cursor c;
	private HashMap<String, String> meMap;
	private String tag = "djevtic";
	private TextView skillName;
	private TextView skillLevel;
	private TextView skillUnclockAt;
	private TextView skillInfo;
	private ProgressBar skillProgress;
	private ImageView weaponImg;
	private TextView weaponKills;
	private TextView weaponDeath;
	private TextView weaponKD;
	private TextView weaponAccuracy;
	private TextView weaponKPM;
	private TextView weaponTime;
	private TextView weaponHSKills;
	private TextView weaponHSoots;
	private TextView weaponName;
	private TextView weaponUsage;
	private TextView damageName;
	private TextView damageKills;
	private TextView perkName;
	private TextView perkKills;
	private TextView perkDeaths;
	private TextView perkTime;
	private TextView perkUsage;
	private TextView perkXp;
	private TextView mapName;
	private ImageView mapImage;
	private TextView mapKills;
	private TextView mapDeaths;
	private TextView mapWon;
	private TextView mapLost;
	private TextView mapDrawn;
	private TextView mapTime;
	private TextView mapXP;
	private TextView mapHighScore;
	private TextView modeName;
	private ImageView modeImage;
	private TextView modeKills;
	private TextView modeDeaths;
	private TextView modeWon;
	private TextView modeLost;
	private TextView modeDrawn;
	private TextView modeTime;
	private TextView modeXP;
	private TextView modeHighScore;
	private TextView suitModeName;
	private TextView suitModeKills;
	private TextView suitModeDeaths;
	private TextView suitModeTime;
	private TextView modeGamesPlayer;
	private TextView mapGamesPlayer;
	
	public GlobalStatsArrayAdaptor(Context context, String[] values,
			String stats) {
		super(context, R.layout.global_stat_content, values);
		this.context = context;
		this.values = values;
		this.player = Player.getInstance();
		this.statsName = stats;
		this.db = new Database(context);
		LoadImageLibs();
	}
	
	public GlobalStatsArrayAdaptor(Context context,String[] values, int stats) {
		super(context, R.layout.global_stat_content, values);
		this.context = context;
		this.values = values;
		this.player = Player.getInstance();
		this.statsName = stats+"";
		LoadImageLibs();
	}

	private void LoadImageLibs() {
		LoaderSettings settings = new SettingsBuilder().withDisconnectOnEveryCall(true).build(context);
		imageManager = new ImageManager(context, settings);
		imageTagFactory = new ImageTagFactory(context,R.drawable.crysis3s_logo);
		imageTagFactory.setErrorImageId(R.drawable.crysis3s_logo);
		imageLoader = imageManager.getLoader();
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		String s = values[position];
		if (statsName.equals("playerselector")) {
			rowView = inflater.inflate(R.layout.playerselectorseparatelayout,parent, false);
			playerRankImage = (ImageView) rowView.findViewById(R.id.playerRankImage);
			playerName = (TextView) rowView.findViewById(R.id.textName);
			playerRank = (TextView) rowView.findViewById(R.id.textRank);
			playerProgress = (TextView) rowView.findViewById(R.id.textScore);
			db.open();
			c = db.getAllPlayers();
			if (s != null) {
				if (c != null) {
					if (c.moveToFirst()) {
						do {
							if (c.getString(c.getColumnIndex("id")) != null) {
								if (s.equals(c.getString(c.getColumnIndex("id")))) {
									setImage(playerRankImage, c.getString(c.getColumnIndex("rankimage")));
									playerName.setText(c.getString(c.getColumnIndex("name")));
									playerRank.setText(c.getString(c.getColumnIndex("rank")));
									String score = c.getString(c.getColumnIndex("curentscore"));
									playerProgress.setText("Curent score: "+score);
								}
							}
						} while (c.moveToNext());
					}
				}
			}
			c.close();
			db.close();
		}else if(statsName.equals("1")){
			rowView = inflater.inflate(R.layout.skillsinglelayout,parent, false);
			skillLevel = (TextView) rowView.findViewById(R.id.textSkillLevel);
			skillUnclockAt = (TextView) rowView.findViewById(R.id.textSkillUnlockAt);
			skillInfo = (TextView) rowView.findViewById(R.id.textSkillInfo);
			skillName = (TextView) rowView.findViewById(R.id.textSkillName);
			skillProgress = (ProgressBar) rowView.findViewById(R.id.progresSkillLevel);
			int index = getSkillIndex(s);
			try {
				skillName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getString("name"));
				if(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getString("unlockrank").equals("null")){
					skillUnclockAt.setText("Unlocks at Rank 0");
				}else{
					skillUnclockAt.setText("Unlocks at Rank "+player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getString("unlockrank"));
				}
			skillLevel.setText("Level "+player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getString("level")+" of "+
					player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getJSONArray("levels").length());
			skillInfo.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getString("msg"));
			skillProgress.setMax(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getInt("totaltarget"));
			skillProgress.setProgress(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(index).getInt("totalcurr"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(statsName.equals("2")){
			rowView = inflater.inflate(R.layout.weaponlayoutsingle,parent, false);
			weaponName = (TextView) rowView.findViewById(R.id.textWeaponsName);
			weaponImg = (ImageView) rowView.findViewById(R.id.imgWeaponImg);
			weaponKills = (TextView) rowView.findViewById(R.id.textWeaponsKills);
			weaponDeath = (TextView) rowView.findViewById(R.id.textWeaponsKDeath);
			weaponKD = (TextView) rowView.findViewById(R.id.textWeaponsKD);
			weaponAccuracy = (TextView) rowView.findViewById(R.id.textWeaponsAccuracy);
			weaponKPM = (TextView) rowView.findViewById(R.id.textWeaponsKPM);
			weaponTime = (TextView) rowView.findViewById(R.id.textWeaponsTime);
			weaponHSKills = (TextView) rowView.findViewById(R.id.textWeaponsHSKills);
			weaponHSoots = (TextView) rowView.findViewById(R.id.textWeaponsHSoots);
			weaponUsage = (TextView) rowView.findViewById(R.id.textWeaponsKUsage);
			int index = getWeaponIndex(s);
			try {
				weaponName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("name"));
				setImage(weaponImg, player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("img"));
				weaponKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("kills"));
				weaponDeath.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("deaths"));
				weaponKD.setText(GetTwoDecimal(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("kills"),player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("deaths"))+"");
				weaponAccuracy.setText(GetAccuracy(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("hits"),player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("shots")));
				weaponKPM.setText(getScorePerMinute(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("kills"),(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("time"))));
				weaponTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getLong("time")));
				weaponHSKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("hskills"));
				weaponHSoots.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("headshots"));
				weaponUsage.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(index).getString("usage"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(statsName.equals("3")){
		rowView = inflater.inflate(R.layout.damagetypesingle,parent, false);
		damageName = (TextView) rowView.findViewById(R.id.textDamageType);
		damageKills = (TextView) rowView.findViewById(R.id.textDamageKills);
		int index = getIndex(s, "damagetypes");
		try {
			damageName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("damagetypes").getJSONObject(index).getString("name"));
			damageKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("damagetypes").getJSONObject(index).getString("kills"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}else if(statsName.equals("4")){
			rowView = inflater.inflate(R.layout.perkssingle,parent, false);
			perkName = (TextView) rowView.findViewById(R.id.textPerkName);
			perkKills = (TextView) rowView.findViewById(R.id.textPerkKills);
			perkDeaths = (TextView) rowView.findViewById(R.id.textPerkDeath);
			perkTime = (TextView) rowView.findViewById(R.id.textPerkTime);
			perkUsage = (TextView) rowView.findViewById(R.id.textPerkUsage);
			perkXp = (TextView) rowView.findViewById(R.id.textPerkXP);
			int index = getIndex(s, "perks");
			try {
				perkName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getString("name"));
				perkKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getString("kills"));
				perkDeaths.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getString("deaths"));
				perkTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getLong("time")));
				perkUsage.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getString("usagecount"));
				perkXp.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("perks").getJSONObject(index).getString("xp"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(statsName.equals("5")){
			rowView = inflater.inflate(R.layout.mapsingle,parent, false);
			mapName = (TextView) rowView.findViewById(R.id.textMapName);
			mapImage = (ImageView) rowView.findViewById(R.id.imgMapImg);
			mapKills = (TextView) rowView.findViewById(R.id.textMapKills);
			mapDeaths = (TextView) rowView.findViewById(R.id.textMapKDeath);
			mapWon = (TextView) rowView.findViewById(R.id.textMapWon);
			mapLost = (TextView) rowView.findViewById(R.id.textMapLost);
			mapDrawn = (TextView) rowView.findViewById(R.id.textMapDrawn);
			mapTime = (TextView) rowView.findViewById(R.id.textMapTime);
			mapXP = (TextView) rowView.findViewById(R.id.textMapXP);
			mapHighScore = (TextView) rowView.findViewById(R.id.textMapHighScore);
			mapGamesPlayer = (TextView) rowView.findViewById(R.id.textMapGames);
			int index = getIndex(s, "maps");
			try {
				mapName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("name"));
				setImage(mapImage, player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("img"));
				mapKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("kills"));
				mapDeaths.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("deaths"));
				mapWon.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("won"));
				mapLost.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("lost"));
				mapDrawn.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("drawn"));
				mapTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getLong("time")));
				mapXP.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("xp"));
				mapHighScore.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("highscore"));
				mapGamesPlayer.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("maps").getJSONObject(index).getString("games"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(statsName.equals("6")){
			rowView = inflater.inflate(R.layout.modessingle,parent, false);
			modeName = (TextView) rowView.findViewById(R.id.textModeName);
			modeKills = (TextView) rowView.findViewById(R.id.textModeKills);
			modeDeaths = (TextView) rowView.findViewById(R.id.textModeKDeath);
			modeWon = (TextView) rowView.findViewById(R.id.textModeWon);
			modeLost = (TextView) rowView.findViewById(R.id.textModeLost);
			modeDrawn = (TextView) rowView.findViewById(R.id.textModeDrawn);
			modeTime = (TextView) rowView.findViewById(R.id.textModeTime);
			modeXP = (TextView) rowView.findViewById(R.id.textModeXP);
			modeHighScore = (TextView) rowView.findViewById(R.id.textModeHighScore);
			modeGamesPlayer = (TextView) rowView.findViewById(R.id.textModeGames);
			int index = getIndex(s, "modes");
			try {
				modeName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("name"));
				modeKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("kills"));
				modeDeaths.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("deaths"));
				modeWon.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("won"));
				modeLost.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("lost"));
				modeDrawn.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("drawn"));
				modeTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getLong("time")));
				modeXP.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("xp"));
				modeHighScore.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("highscore"));
				modeGamesPlayer.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("modes").getJSONObject(index).getString("games"));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(statsName.equals("7")){
			rowView = inflater.inflate(R.layout.suitmodesingle,parent, false);
			suitModeName = (TextView) rowView.findViewById(R.id.textSuitName);
			suitModeKills = (TextView) rowView.findViewById(R.id.textSuitKills);
			suitModeDeaths = (TextView) rowView.findViewById(R.id.textSuitDeath);
			suitModeTime = (TextView) rowView.findViewById(R.id.textSuitTime);
			int index = getIndex(s, "suitmodes");
			try {
				suitModeName.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("suitmodes").getJSONObject(index).getString("name"));
				suitModeKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("suitmodes").getJSONObject(index).getString("kills"));
				suitModeDeaths.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("suitmodes").getJSONObject(index).getString("deaths"));
				suitModeTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("suitmodes").getJSONObject(index).getLong("time")));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return rowView;
	}

	private int getIndex(String s, String what) {
		int index = 0;
		int arrayLenght = 0;
		try {
			arrayLenght = player.getPlayerJSONObject().getJSONObject("stats").getJSONArray(what).length();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < arrayLenght; i++) {
			try {
				if(s.equals(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray(what).getJSONObject(i).getString("id"))){
					index = i;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return index;
	}

	private int getWeaponIndex(String s) {
		int index = 0;
		int arrayLenght = 0;
		try {
			arrayLenght = player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").length();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < arrayLenght; i++) {
			try {
				if(s.equals(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("weapons").getJSONObject(i).getString("id"))){
					index = i;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return index;
	}

	private int getSkillIndex(String s) {
		int index = 0;
		int arrayLenght = 0;
		try {
			arrayLenght = player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").length();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = 0; i < arrayLenght; i++) {
			try {
				if(s.equals(player.getPlayerJSONObject().getJSONObject("stats").getJSONArray("skills").getJSONObject(i).getString("id"))){
					index = i;
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return index;
	}
	
	private String getUnlockingString(int curent, int needed) {
		if (curent < needed) {
			return "" + curent + "/" + needed;
		} else {
			return "" + needed + "/" + needed;
		}
	}
	
	public long checkAndDivide(int one, int two){
		long result = 0;
		if(two!=0){
			result = one/two;
		}
		return result;
	}

	public HashMap weaponHashMap() {

		meMap = new HashMap<String, String>();
		return meMap;
	}

	public String getTime(long timeLong) {
		String timeString = "";
		int seconds = (int) (timeLong) % 60;
		int minutes = (int) ((timeLong / 60) % 60);
		int hours = (int) ((timeLong / (60 * 60)) % 24);
		int days = (int) ((timeLong / (60 * 60 * 24)) % 365);
		if (days > 0) {
			timeString = timeString + days + "d ";
		}
		if (hours > 0) {
			timeString = timeString + hours + "h ";
		}
		timeString = timeString + minutes + "m " + seconds + "s ";
		return timeString;
	}

	private void setImage(ImageView imageView, String url) {
		Log.v(tag ,"ImageView == "+imageView);
		Log.v(tag ,"url == "+url);
		String link = "https://dl.dropboxusercontent.com/u/1480498/crysis3/" + url;
		ImageTag tag = imageTagFactory.build(link);
		imageView.setTag(tag);
		imageLoader.load(imageView);
	}
	
	public String GetTwoDecimal(long first, long secound){
		 if(secound!=0){
			 double d;
			 d = (double)first/(double)secound;
			 DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
			 return f.format(d);
		 }
		 return 0+"";
	}
	
	public String GetAccuracy(long first, long secound){
		if(secound!=0){
			double d;
			d = ((double)first/(double)secound)*100;
			DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
			return f.format(d);
		}
		return 0+"";
	}
	
	private String getScorePerMinute(long playerScore, long playerGameTime) {
		if((playerGameTime/60)!=0){
			long gameTimeInMinutes = playerGameTime/60;
			String scorePerMinute = GetTwoDecimal(playerScore,gameTimeInMinutes);
			return scorePerMinute;
		}
		return 0+"";
	}
	
}
