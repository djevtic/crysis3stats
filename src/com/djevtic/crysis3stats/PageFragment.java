package com.djevtic.crysis3stats;

import java.text.DecimalFormat;

import org.json.JSONException;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class PageFragment extends Fragment{

	public static PageFragment newInstance(int page) {
		  
        PageFragment pageFragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageFragment.setArguments(bundle);
        return pageFragment;
    }

	public Player player = Player.getInstance();

	private String tag = "djevtic";
	
	
	private TextView gamesPlayed;
	private TextView wins;
	private TextView draws;
	private TextView losses;
	private TextView score;
	private TextView kills;
	private TextView deaths;
	private TextView killDeath;
	private TextView winLose;
	private TextView winStreak;
	private TextView loseStreak;
	private TextView highestXP;
	private TextView overalTime;
	private TextView dogtagsColected;
	private TextView dogtagsUnlocked;
	private TextView modulUnlocked;
	private TextView skillACompleted;
	private TextView threDogtagsCollected;
	private TextView allEnemyDogtags;
	private TextView chellengesCompleted;
	private TextView energyUsed;
	private TextView lifeExpectancy;

	private TextView gunshipCall;

	private TextView gunshipKills;

	private TextView flagCaptured;

	private TextView flagCarrierKills;

	private TextView frendlyFire;

	private TextView captureObjectivs;

	private TextView carryObjectives;

	private TextView detonationsDelay;

	private TextView extractionDefendingKills;

	private TextView finalIntel;

	private TextView tagAssist;

	private TextView tagedAndbaged;

	private TextView taggedEntries;

	private TextView teamRadar;

	private TextView teakeLateFlag;

	private TextView wonCTF;

	private TextView wonExtraction;

	private TextView empBlast;

	private TextView swarmerActivation;

	private TextView allSpearsCaptured;

	private TextView taggedEnemyTeam;

	private TextView enemyTeamKilled;

	private TextView c4Attached;

	private TextView flagCarriedTime;

	private TextView intelCollectedTime;

	private TextView maneuveroubilityKills;

	private TextView rapidFireKills;

	private TextView killsWithoutAssist;

	private TextView loneWolfKills;

	private TextView killsWithWeaponPerks;

	private TextView skillKill;

	private TextView reflexSightKills;

	private TextView safetyInNumbersKills;

	private TextView cloackEnemyKills;

	private TextView warBirdKills;

	private TextView doubleKill;

	private TextView pointFireEnhanceKill;

	private TextView treckerKill;

	private TextView weaponProKills;

	private TextView sniperScopedKills;

	private TextView killJoyKills;

	private TextView rumbledKills;

	private TextView unmountedKills;

	private TextView heavyWeaponKills;

	private TextView airKills;

	private TextView meleeKills;

	private TextView airDeathKills;

	private GlobalStatsArrayAdaptor adapter;

	private ListView list;
	private String[] skillIDs;

	private ImageView bfstatimage;
      
    @Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
      
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        View view = null;
        if(page ==0){
        	view = inflater.inflate(R.layout.playerinfo, container, false);
        	fillPage(page,view);
        	loadAdds(view);
        }else if(page==1){
        	view = inflater.inflate(R.layout.teamstats, container, false);
        	fillPage(page,view);
        	loadAdds(view);
        }else if(page==2){
        	view = inflater.inflate(R.layout.playerkills, container, false);
        	fillPage(page,view);
        	loadAdds(view);
        }else if(page==10){
        	view = inflater.inflate(R.layout.about, container, false);
        	fillPage(page,view);
        	addListenerOnButton(view);
            addImageOnClickListener(view);
        }
        return view;  
    }

    private void loadAdds(View view) {
    	AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
	}
    
	private void fillPage(int page, View view) {
		if(page==0){
			gamesPlayed = (TextView) view.findViewById(R.id.textGamePlayed);
			draws = (TextView) view.findViewById(R.id.textDraws);
			score = (TextView) view.findViewById(R.id.textScore);
			kills = (TextView) view.findViewById(R.id.textKils);
			deaths = (TextView) view.findViewById(R.id.textDeaths);
			killDeath = (TextView) view.findViewById(R.id.textKD);
			winLose = (TextView) view.findViewById(R.id.textWL);
			lifeExpectancy = (TextView) view.findViewById(R.id.textLifeExpectancy);
			wins = (TextView) view.findViewById(R.id.textWin);
			losses = (TextView) view.findViewById(R.id.textLosses);
			winStreak = (TextView) view.findViewById(R.id.textWinStreak);
			loseStreak = (TextView) view.findViewById(R.id.textLoseStreak);
			highestXP = (TextView) view.findViewById(R.id.textHighestXP);
			overalTime = (TextView) view.findViewById(R.id.textOveralTime);
			dogtagsColected = (TextView) view.findViewById(R.id.textDogtagsCollected);
			dogtagsUnlocked = (TextView) view.findViewById(R.id.textDogtagsUnlocked);
			modulUnlocked = (TextView) view.findViewWithTag(R.id.textModulUnlocked);
			skillACompleted = (TextView) view.findViewById(R.id.textSkillACompleted);
			threDogtagsCollected = (TextView) view.findViewById(R.id.text3DogtagsColected);
			allEnemyDogtags = (TextView) view.findViewById(R.id.textAllEnemyDogtags);
			chellengesCompleted = (TextView) view.findViewById(R.id.textChalengesCompleted);
			energyUsed = (TextView) view.findViewById(R.id.textEnergyUsed);
			
			try {
				gamesPlayed.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("gamesplayed")+"");
				wins.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("gameswon")+"");
				losses.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("gameslost")+"");
				draws.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("gamesdrawn")+"");
				score.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("rank").getInt("score")+"");
				kills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("kills")+"");
				deaths.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("deaths")+"");
				killDeath.setText(GetTwoDecimal(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("kills"),player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("deaths")));
				winLose.setText(GetTwoDecimal(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("gameswon"),player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("gameslost")));
				winStreak.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WinStreak")+"");
				loseStreak.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("LoseStreak")+"");
				highestXP.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("highscore")+"");
				overalTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("overallTime")));
				dogtagsColected.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("DogtagsCollected")+"");
				dogtagsUnlocked.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("DogtagsUnlocked")+"");
				//modulUnlocked.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("moduleUnlocks")+"");
				skillACompleted.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("skillAssessmentsCompleted")+"");
				threDogtagsCollected.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("Num3DogtagsCollected")+"");
				allEnemyDogtags.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AllEnemyDogTagsCollected")+"");
				chellengesCompleted.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("ChallengesCompleted")+"");
				energyUsed.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("EnergyUsed")+"");
				lifeExpectancy.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("lifeExpectancy")));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(page == 1){
			gunshipCall = (TextView) view.findViewById(R.id.textGunshipCalls);
			gunshipKills = (TextView) view.findViewById(R.id.textGunshipKills);
			flagCaptured = (TextView) view.findViewById(R.id.textFlagCaptured);
			flagCarrierKills = (TextView) view.findViewById(R.id.textFlagCarierKills);
			frendlyFire = (TextView) view.findViewById(R.id.textFriendlyFire);
			captureObjectivs = (TextView) view.findViewById(R.id.textCaptureObjectives);
			carryObjectives = (TextView) view.findViewById(R.id.textCarryObjectives);
			detonationsDelay = (TextView) view.findViewById(R.id.textDetonationDelays);
			extractionDefendingKills = (TextView) view.findViewById(R.id.textExtractionDefendingKills);
			finalIntel = (TextView) view.findViewById(R.id.textFinalIntelWithin5secRemaining);
			tagAssist = (TextView) view.findViewById(R.id.textTagAssist);
			tagedAndbaged = (TextView) view.findViewById(R.id.textTaggedAndBagged);
			taggedEntries = (TextView) view.findViewById(R.id.textTaggedEntities);
			teamRadar = (TextView) view.findViewById(R.id.textTeamRadar);
			teakeLateFlag = (TextView) view.findViewById(R.id.textTakeLateFlagCaptureLead);
			wonCTF = (TextView) view.findViewById(R.id.textWonCTFkeptallflags);
			wonExtraction = (TextView) view.findViewById(R.id.textWonExtractionnogiveup);
			swarmerActivation = (TextView) view.findViewById(R.id.textSwarmeractivations);
			empBlast = (TextView) view.findViewById(R.id.textEMPBlastactivations);
			allSpearsCaptured =(TextView) view.findViewById(R.id.textAllSpearscaptured);
			taggedEnemyTeam = (TextView) view.findViewById(R.id.textTaggedentireenemyteam);
			enemyTeamKilled = (TextView) view.findViewById(R.id.textEnemyteamkilledinXseconds);
			c4Attached = (TextView) view.findViewById(R.id.textC4attachedtoteamkills);
			flagCarriedTime = (TextView) view.findViewById(R.id.textFlagcarriedtime);
			intelCollectedTime = (TextView) view.findViewById(R.id.textIntelcollectedtime);
			
			try {
				gunshipCall.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AlienGunshipCalls")+"");
				gunshipKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AlienGunshipKills")+"");
				flagCaptured.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("FlagCaptures")+"");
				flagCarrierKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("FlagCarrierKills")+"");
				frendlyFire.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("FriendlyFires")+"");
				captureObjectivs.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("CaptureObjectives")+"");
				carryObjectives.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("CarryObjectives")+"");
				detonationsDelay.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("DetonationDelays")+"");
				extractionDefendingKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WonExtractDefendingNoGiveUp")+"");
				finalIntel.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("FinalIntel5SecRemaining")+"");
				tagAssist.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TagAssist")+"");
				tagedAndbaged.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TaggedAndBagged")+"");
				taggedEntries.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TaggedEntities")+"");
				teamRadar.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TeamRadar")+"");
				teakeLateFlag.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TakeLateFlagCaptureLead")+"");
				wonCTF.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WonCTFWithoutGivingUpAScore")+"");
				wonExtraction.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WonExtractDefendingNoGiveUp")+"");
				swarmerActivation.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("SwarmerActivations")+"");
				empBlast.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("EMPBlastActivations")+"");
				allSpearsCaptured.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AllSpearsCaptured")+"");
				taggedEnemyTeam.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TaggedEntireEnemyTeam")+"");
				enemyTeamKilled.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("EnemyTeamKilledInXSeconds")+"");
				c4Attached.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("C4AttachedToTeamMateKills")+"");
				flagCarriedTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("FlagCarriedTime")));
				intelCollectedTime.setText(getTime(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getLong("IntelCollectedTime")));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(page == 2){
			killsWithoutAssist = (TextView) view.findViewById(R.id.textKillswithoutassist);
			maneuveroubilityKills = (TextView) view.findViewById(R.id.textManeuverabilitykills);
			rapidFireKills = (TextView) view.findViewById(R.id.textRapidfirekills);
			loneWolfKills = (TextView) view.findViewById(R.id.textLonewolfkills);
			killsWithWeaponPerks =(TextView) view.findViewById(R.id.textKillswithweaponPerks);
			skillKill  =(TextView) view.findViewById(R.id.textSkillkills);
			reflexSightKills  =(TextView) view.findViewById(R.id.textReflexsightkills);
			safetyInNumbersKills  =(TextView) view.findViewById(R.id.textSafetyinnumberskills);
			cloackEnemyKills  =(TextView) view.findViewById(R.id.textCloakedenemykills);
			warBirdKills  =(TextView) view.findViewById(R.id.textWarbirdkills);
			doubleKill  =(TextView) view.findViewById(R.id.textDoublekills);
			pointFireEnhanceKill  =(TextView) view.findViewById(R.id.textPointfireenhancekills);
			treckerKill  =(TextView) view.findViewById(R.id.textTrackerkills);
			weaponProKills  =(TextView) view.findViewById(R.id.textWeaponprokillsafterreload);
			sniperScopedKills  =(TextView) view.findViewById(R.id.textSniperscopedkills);
			killJoyKills  =(TextView) view.findViewById(R.id.textKilljoykills);
			rumbledKills  =(TextView) view.findViewById(R.id.textRumbledkills);
			unmountedKills  =(TextView) view.findViewById(R.id.textUnmountedkills);
			heavyWeaponKills  =(TextView) view.findViewById(R.id.textHeavyweaponkills);
			airKills  =(TextView) view.findViewById(R.id.textAirkills);
			meleeKills  =(TextView) view.findViewById(R.id.textMeleekills);
			airDeathKills  =(TextView) view.findViewById(R.id.textAirdeathkills);
		
			try {
				killsWithoutAssist.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("KillsWithoutAssist")+"");
				maneuveroubilityKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("MobilityKills")+"");
				rapidFireKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("RapidFireKills")+"");
				loneWolfKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("LoneWolfKills")+"");
				killsWithWeaponPerks.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("KillsWithAllWeaponPerks")+"");
				skillKill.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("SkillKills")+"");
				reflexSightKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("ReflexSightKills")+"");
				safetyInNumbersKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("SafetyInNumbersKills")+"");
				cloackEnemyKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("NumCloakedVictimKills")+"");
				warBirdKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WarBirdKills")+"");
				doubleKill.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("DoubleKills")+"");
				pointFireEnhanceKill.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("PointFireEnhanceKills")+"");
				treckerKill.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("TrackerKills")+"");
				weaponProKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("WeaponProKillsAfterReload")+"");
				sniperScopedKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("SniperScopeKills")+"");
				killJoyKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("KillJoyKills")+"");
				rumbledKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("RumbledKills")+"");
				unmountedKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("UnmountedKills")+"");
				heavyWeaponKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("HeavyWeaponKills")+"");
				airKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AirKillKills")+"");
				meleeKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("MeleeTakeDownKills")+"");
				airDeathKills.setText(player.getPlayerJSONObject().getJSONObject("stats").getJSONObject("general").getInt("AirDeathKills")+"");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void setSkillIDs() {
		skillIDs = new String[]{"sa1", "sa2", "sa3", "sa4", "sa5", "sa6", "sa7", "sa8", 
				"sa9", "sa10", "sa11", "sa12", "sa13", "sa14", "sa15", "sa16", "sa17", 
				"sa18", "sa19", "sa20", "sa21", "sa22", "sa23", "sa24", "sa25", "sa26", "sa27", 
				"sa28", "sa29", "sa30", "sa31", "sa32", "sa33", "sa34", "sa35", "sa36", "sa37", "sa38", 
				"sa39", "sa40", "sa41", "sa42", "sa43", "sa44", "sa45", "sa46", "sa47", "sa48", "sa49", "sa50", "sa51", "sa52",
				"sa53", "sa54", "sa55", "sa56", "sa57", "sa58", "sa59", "sa60", "sa61", "sa62", "sa63", "sa64", "sa65", "sa66", 
				"sa67", "sa68", "sa69", "sa70", "sa71", "sa72", "sa73", "sa74", "sa75", "sa76", "sa77", "sa78", "sa79", "sa80", 
				"sa81", "sa82", "sa83", "sa84", "sa85", "sa86", "sa87", "sa88", "sa89", "sa90", "sa91", "sa92", "sa93", "sa94", 
				"sa95", "sa96", "sa97", "sa98", "sa99", "sa100", "sa101", "sa102", "sa103", "sa104", "sa105", "sa106", "sa107", 
				"sa108", "sa109", "sa110", "sa111", "sa112", "sa113", "sa114", "sa115", "sa116", "sa117", "sa118", "sa119", "sa120", 
				"sa121", "sa122", "sa123", "sa124", "sa125", "sa126", "sa127", "sa128", "sa129", "sa130", "sa131", "sa132", "sa133", 
				"sa134", "sa135", "sa136", "sa137", "sa138", "sa139", "sa140", "sa141", "sa142", "sa143", "sa144", "sa145", "sa146", 
				"sa147", "sa148", "sa149", "sa150", "sa151", "sa152", "sa153", "sa154", "sa155", "sa156", "sa157", "sa158", "sa159", 
				"sa160", "sa161", "sa162", "sa163", "sa164", "sa165", "sa166", "sa167", "sa168", "sa169", "sa170", "sa171", "sa172", 
				"sa173", "sa174", "sa175", "sa176", "sa177", "sa178", "sa179", "sa180", "sa181", "sa182", "sa183", "sa184", "sa185", 
				"sa186", "sa187", "sa188", "sa189", "sa190", "sa191", "sa192", "sa193", "sa194", "sa195", "sa196", "sa197", "sa198", 
				"sa199", "sa200", "sa201", "sa202", "sa203", "sa204", "sa205", "sa206", "sa207", "sa208", "sa209", "sa210", "sa211", 
				"sa212", "sa213", "sa214", "sa215", "sa216", "sa217", "sa218", "sa219", "sa220", "sa221", "sa222", "sa223", "sa224", 
				"sa225", "sa226", "sa227", "sa228", "sa229", "sa230", "sa231", "sa232", "sa233", "sa234", "sa235", "sa236", "sa237", 
				"sa238", "sa239", "sa240", "sa241", "sa242", "sa243", "sa244", "sa245", "sa246", "sa247", "sa248", "sa249", "sa250", 
				"sa251", "sa252", "sa253", "sa254", "sa255", "sa256", "sa257", "sa258", "sa259", "sa260", "sa261", "sa262", "sa263", 
				"sa264", "sa265", "sa266", "sa267", "sa268", "sa269", "sa270", "sa271", "sa272", "sa273", "sa274", "sa275", "sa276", 
				"sa277", "sa278", "sa279", "sa280", "sa281", "sa282", "sa283", "sa284"};
	}

	public String getTime(long timeLong) {
		String timeString = "";
		int seconds = (int) (timeLong) % 60;
		int minutes = (int) ((timeLong / 60) % 60);
		int hours = (int) ((timeLong / (60 * 60)) % 24);
		int days = (int) ((timeLong / (60 * 60 * 24)) % 365);
		if (days > 0) {
			timeString = timeString + days + "d ";
		}
		if (hours > 0) {
			timeString = timeString + hours + "h ";
		}
		timeString = timeString + minutes + "m " + seconds + "s ";
		return timeString;
	}
	
	private void addImageOnClickListener(View view) {
		bfstatimage = (ImageView) view.findViewById(R.id.bfstatimage);
		bfstatimage.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://crysis3s.com/"));
				startActivity(urlIntent);
			}
 
		});
	}
	
	public void addListenerOnButton(View view) {
		 
		Button emailButton = (Button) view.findViewById(R.id.email_button);
		Button marketButton = (Button) view.findViewById(R.id.market_button);
		Button donateButton = (Button) view.findViewById(R.id.donate_button);
 
		emailButton.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View arg0) {
 
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("message/rfc822");
				i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"djevtic@dragisajevtic.com"});
				i.putExtra(Intent.EXTRA_SUBJECT, "Crysis 3 Stats");
				i.putExtra(Intent.EXTRA_TEXT   , "Enter your text here");
				try {
				    startActivity(Intent.createChooser(i, "Send mail..."));
				} catch (android.content.ActivityNotFoundException ex) {
				    Toast.makeText(getActivity(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
				}
 
			}
 
		});
		
		marketButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=com.djevtic.crysis3stats"));
				startActivity(intent);
 
			}
 
		});
		
		donateButton.setOnClickListener(new OnClickListener() {
			 
			@Override
			public void onClick(View arg0) {
 
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N3D359RYJBY54"));
				startActivity(intent);
 
			}
 
		});
 
	}
	
	public String GetTwoDecimal(long first, long secound){
		 
        double d;
        d = (double)first/(double)secound;
        DecimalFormat f = new DecimalFormat("#0.00");  // this will helps you to always keeps in two decimal places
        return f.format(d); 
	}
}
