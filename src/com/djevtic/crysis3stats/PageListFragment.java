package com.djevtic.crysis3stats;

import org.json.JSONException;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PageListFragment extends ListFragment{
	
	public static PageListFragment newInstance(int page) {
		  
		PageListFragment pageListFragment = new PageListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key", page);
        pageListFragment.setArguments(bundle);
        return pageListFragment;
    }
	private Player player;
	private String tag = "djevtic";
	private GlobalStatsArrayAdaptor adapter;
	private String[] skillIDs;
	private String[] weaponIDs;

	@Override  
    public void onCreate(Bundle savedInstanceState) {  
        super.onCreate(savedInstanceState);  
    }  
      
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {  
        int page = getArguments().getInt("key");
        player = Player.getInstance();
        View view = null;
        	view = inflater.inflate(R.layout.statistics, container, false);
        	fillPage(page,view);
        	loadAdds(view);
        
        return view;  
    }

    private void loadAdds(View view) {
    	AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
	}
    
	private void fillPage(int page, View view) {
		if(page==3){
			Log.v(tag ,"we got 3");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("skills"), 1);
			setListAdapter(adapter);
		}else if(page==4){
			Log.v(tag ,"we got 4");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("weapons"), 2);
			setListAdapter(adapter);
		}else if(page==5){
			Log.v(tag ,"we got 5");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("damagetypes"), 3);
			setListAdapter(adapter);
		}else if(page==6){
			Log.v(tag ,"we got 6");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("perks"), 4);
			setListAdapter(adapter);
		}else if(page==7){
			Log.v(tag ,"we got 7");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("maps"), 5);
			setListAdapter(adapter);
		}else if(page==8){
			Log.v(tag ,"we got 8");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("modes"), 6);
			setListAdapter(adapter);
		}else if(page==9){
			Log.v(tag ,"we got 9");
			adapter = new GlobalStatsArrayAdaptor(getActivity(), checkType("suitmodes"), 7);
			setListAdapter(adapter);
		}
		
	}
	
	private String[] checkType(String s) {
		int numberofElements = 0;
		try {
			numberofElements = player.getPlayerJSONObject().getJSONObject("stats").getJSONArray(s).length();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String[] list = new String[numberofElements];
		for(int i=0;i<numberofElements;i++){
			try {
				list[i]=player.getPlayerJSONObject().getJSONObject("stats").getJSONArray(s).getJSONObject(i).getString("id");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}
}
